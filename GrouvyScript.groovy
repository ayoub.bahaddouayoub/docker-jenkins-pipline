
    
    def test(){
            echo "this is the testl step with function define inside scrip.groovy "
        }
    
    def buildJarFile(){
            echo "this is the build jar file step with function define inside scrip.groovy using $BRANCH_NAME "
            sh "mvn clean package"
        }

    def buildDockerImage(){
        echo "build Docker image with function define inside scrip.groovy ${IMAGE_NAME}"
        sh "docker build -t bahaddou88/my_ci_repo:v1.0.3 ."
        withCredentials([usernamePassword(credentialsId: 'docker-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]){
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push bahaddou88/my_ci_repo:v1.0.3"
    }
    
     }

    def deploy(){
            echo "this is the deploy step with function define inside scrip.groovy"
            def dockerCommand="docker run -d -p 8888:8888 bahaddou88/my_ci_repo:v1.0.3"
            sshagent(['aws-credentials-account']) {
                sh "ssh -o StrictHostKeyChecking=no  ec2-user@3.122.104.117 ${dockerCommand}"
    }
        }

    return this
